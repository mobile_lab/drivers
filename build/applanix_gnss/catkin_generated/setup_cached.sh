#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/gavriel/Autoware1/src/drivers/awf_drivers/build/applanix_gnss/devel:$CMAKE_PREFIX_PATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/gavriel/Autoware1/src/drivers/awf_drivers/build/applanix_gnss/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/gavriel/Autoware1/src/drivers/awf_drivers/applanix_gnss:$ROS_PACKAGE_PATH"