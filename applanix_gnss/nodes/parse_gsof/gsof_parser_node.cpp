#include <ros/ros.h>

//#include "nmea2tfpose_core.h"

#include <nmea_msgs/Sentence.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/AccelStamped.h>
#include <std_msgs/UInt32.h>
#include <tf/transform_broadcaster.h>
#include "gsofParser.h"
#include "geo_pos_conv.h"

#define PI (3.14159265358979)

double deg2rad(double deg_val)
{
  return deg_val*PI/180;
}
void publishTime(ros::Publisher pub_week,ros::Publisher pub_time ,unsigned long week, unsigned long gps_time)
{
  pub_week.publish(week);
  pub_time.publish(gps_time);
}

void publishPoseStamped(ros::Publisher pub,ros::Time current_time,double x,double y,double z, double roll, double pitch, double yaw)
{
  geometry_msgs::PoseStamped pose;
  pose.header.frame_id = "map";
  pose.header.stamp = current_time;
  pose.pose.position.x = y;
  pose.pose.position.y = x;
  pose.pose.position.z = z;
  //ROS_WARN_STREAM("x: "<<pose.pose.position.x<<" y: "<<pose.pose.position.y<<" z: "<<pose.pose.position.z);
  pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(roll, pitch, yaw);
  pub.publish(pose);
}

void publishTF(tf::TransformBroadcaster br,ros::Time current_time,double x,double y,double z, double roll, double pitch, double yaw)
{
  tf::Transform transform;
  transform.setOrigin(tf::Vector3(y, x, z));
  tf::Quaternion quaternion;
  quaternion.setRPY(roll, pitch, yaw);
  transform.setRotation(quaternion);
  br.sendTransform(tf::StampedTransform(transform, current_time, "gnss_frame","gps" ));
}
void publishVelStamped(ros::Publisher vel_pub,ros::Time current_time,double angular_x,double angular_y,double angular_z, double linear_x, double linear_y, double linear_z,double total_speed)
{
  //geometry_msgs::Twist twist;
  //twist.
  geometry_msgs::TwistStamped ts;
  ts.header.stamp = current_time;
  ts.twist.angular.x = angular_x;
  ts.twist.angular.y = angular_y;
  ts.twist.angular.z = angular_z;
  // ts.twist.linear.x = linear_x;
  // ts.twist.linear.y = linear_y;
  // ts.twist.linear.z = linear_z;
  ts.twist.linear.x = total_speed;
  ts.twist.linear.y = 0;
  ts.twist.linear.z = 0;
  vel_pub.publish(ts);
}
void publishAccelStamped(ros::Publisher accel_pub,ros::Time current_time,double angular_x,double angular_y,double angular_z, double linear_x, double linear_y, double linear_z)
{
  //geometry_msgs::Accel accel;
  //twist.
  geometry_msgs::AccelStamped as;
  as.header.stamp = current_time;
  as.accel.angular.x = angular_x;
  as.accel.angular.y = angular_y;
  as.accel.angular.z = angular_z;
  as.accel.linear.x = linear_x;
  as.accel.linear.y = linear_y;
  as.accel.linear.z = linear_z;
  accel_pub.publish(as);
}
class gsofHandler
{
  public:
  gsofHandler();
  ~gsofHandler();
  private:
  ros::NodeHandle n;
  void callbackFromGsofSentence(const nmea_msgs::Sentence::ConstPtr &msg);
  ros::Subscriber sub_gsof_;
  ros::Publisher pub_pose_;
  ros::Publisher vel_pub_,acc_pub_;
  ros::Publisher pub_week,pub_time;
  tf::TransformBroadcaster br;
  ros::Time current_time;
  gsofParser Parser;//paser for gsof
  geo_pos_conv geo;//convert from lat-lon to x-y


};

gsofHandler::gsofHandler()
{
  geo.set_plane(32.1028, 35.2094);//tmp
  sub_gsof_ = n.subscribe("gsof_sentence", 100, &gsofHandler::callbackFromGsofSentence, this);
  pub_pose_ = n.advertise<geometry_msgs::PoseStamped>("gnss_pose", 10);//gnss_pose
  vel_pub_ = n.advertise<geometry_msgs::TwistStamped>("gnss_velocity", 10);
  acc_pub_ = n.advertise<geometry_msgs::AccelStamped>("gnss_acceleration", 10);
  pub_week = n.advertise<std_msgs::UInt32>("gps_week", 10);
  pub_time = n.advertise<std_msgs::UInt32>("gps_time", 10);
  ros::spin();
}
gsofHandler::~gsofHandler()
{
  
}

void gsofHandler::callbackFromGsofSentence(const nmea_msgs::Sentence::ConstPtr &msg)
{
  current_time = msg->header.stamp;//ros::Time::now();//
  InsFullNavigationInfo navInfo;
  int complete_gsof = Parser.postGsofData((unsigned char*)msg->sentence.c_str(),navInfo);
  //ROS_WARN_STREAM(" lat: "<<navInfo.Latitude<<" lon: "<<navInfo.Longitude<<" navInfo.Altitude: "<<navInfo.Altitude<<" complete_gsof: "<<complete_gsof);

  if (complete_gsof >0)//if (complete_gsof == 1)//BUG
  {
    
    navInfo.Roll = deg2rad(navInfo.Roll);
    navInfo.Pitch = -deg2rad(navInfo.Pitch);
    navInfo.Heading = -deg2rad(navInfo.Heading)+PI/2;
    navInfo.TrackAngle = -deg2rad(navInfo.TrackAngle);

    navInfo.AngularRate_x = deg2rad(navInfo.AngularRate_x);
    navInfo.AngularRate_y = -deg2rad(navInfo.AngularRate_y);
    navInfo.AngularRate_z = -deg2rad(navInfo.AngularRate_z);

    //navInfo.Longitudinal_acceleration = 
    navInfo.Traverse_acceleration = -navInfo.Traverse_acceleration;
    navInfo.Down_acceleration = -navInfo.Down_acceleration;


    //ROS_WARN_STREAM(" lat: "<<navInfo.Latitude<<" lon: "<<navInfo.Longitude<<" navInfo.Altitude: "<<navInfo.Altitude);
    geo.set_llh(navInfo.Latitude, navInfo.Longitude, navInfo.Altitude);
    //ROS_WARN_STREAM("x: "<<geo.x()<<" y: "<<geo.y()<<" z: "<<geo.z());
    // publishPoseStamped(pub_pose_,current_time,geo.x(),geo.y(),geo.z(), navInfo.Roll, navInfo.Pitch, navInfo.Heading);
    // publishTF(br,current_time,geo.x(),geo.y(),geo.z(), navInfo.Roll, navInfo.Pitch, navInfo.Heading);
    publishPoseStamped(pub_pose_,current_time,geo.x(),geo.y(),geo.z(), navInfo.Roll, navInfo.Pitch, navInfo.Heading);
    publishTF(br,current_time,geo.x(),geo.y(),geo.z(), navInfo.Roll, navInfo.Pitch, navInfo.Heading);
    //ROS_WARN_STREAM("roll: "<<navInfo.Roll<<" pitch: "<< navInfo.Pitch<<" yaw: "<<navInfo.Heading);

    publishVelStamped(vel_pub_,current_time,navInfo.AngularRate_x,navInfo.AngularRate_y,navInfo.AngularRate_z, navInfo.NorthVelocity, navInfo.EastVelocity, navInfo.DownVelocity, navInfo.TotalSpeed);
    publishAccelStamped(acc_pub_,current_time,0,0,0, navInfo.Longitudinal_acceleration, navInfo.Traverse_acceleration, navInfo.Down_acceleration);
    publishTime(pub_week,pub_time,navInfo.GPS_Week_Number,navInfo.GPS_Time);
    //publish all;
  }
  else if (complete_gsof == -1)
  ROS_ERROR_STREAM("Error - parsing gsof");

}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "gsof_parser");
  gsofHandler();

  return 0;
}