//sender - subsribed to acceleration, braking, steering, indicators, neutralize commands. create a can massage for the Idan drive by wire system 
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <ros/ros.h>
#include "autoware_can_msgs/CANPacket.h"
// #include "autoware_msgs/SteerCmd.h"
// #include "autoware_msgs/AccelCmd.h"
//#include "autoware_msgs/BrakeCmd.h"
#include <std_msgs/Float64.h>

#include "sender.h"

class IdanSender
{
  public:
  IdanSender();
  ~IdanSender();
private:
  ros::NodeHandle n;
  ros::Subscriber steer_sub;
  ros::Subscriber accel_sub;
  ros::Subscriber brake_sub;  
  ros::Publisher can_pub;
  void steerCallback(const std_msgs::Float64::ConstPtr & msg);
  void accelCallback(const std_msgs::Float64::ConstPtr & msg);
  //void brakeCallback(const autoware_msgs::BrakeCmdConstPtr & msg);
  convertIdanData Conv;
  autoware_can_msgs::CANPacket candat;  
  sendingData sData = sendingData();
  void convertToCan();

};
IdanSender::IdanSender()
{
  steer_sub = n.subscribe("steer_cmd", 1000, &IdanSender::steerCallback, this);
  accel_sub = n.subscribe("accel_cmd", 1000, &IdanSender::accelCallback, this);

  can_pub = n.advertise<autoware_can_msgs::CANPacket>("can_raw_cmd", 10);
  //brake_sub = n.subscribe("brake_cmd", 1000, &IdanSender::brakeCallback, this);
  ros::spin();
}
IdanSender::~IdanSender()
{
}
void IdanSender::steerCallback(const std_msgs::Float64::ConstPtr & msg)
{
  //sData.stamp = msg->header.stamp;
  sData.steer =  msg->data;
  //ROS_INFO_STREAM("steer: "<<sData.steer);
  //candat.header.stamp= msg->header.stamp;
  convertToCan();
}
void IdanSender::accelCallback(const std_msgs::Float64::ConstPtr & msg)
{
  //sData.stamp = msg->header.stamp;
  sData.acc =  msg->data;
  //ROS_INFO_STREAM("accel: "<<sData.acc);
  //candat.header.stamp= msg->header.stamp;
  convertToCan();
}
// void IdanSender::brakeCallback(const autoware_msgs::BrakeCmdConstPtr & msg)
// {
//   ROS_INFO_STREAM("brake: "<<msg->brake);
// }
void IdanSender::convertToCan()
{
  unsigned char  idan_sData[8] = { 0 };

  Conv.convert_to_idan(&sData, idan_sData);
  candat.count =0;//number of sended message
	candat.time=0;//time of data creation
	candat.id = 0x30;//
	candat.len = 8;//message lenght?
	candat.header.stamp= ros::Time::now();
  for (int j = 0; j < 8; j++)
  {
    //printf("%2.2x ", idan_sData[j]);
	  candat.dat[j]=idan_sData[j];
	}
	can_pub.publish(candat);


}


 
int main (int argc, char *argv[])

{
  ros::init(argc, argv, "idan_sender");
IdanSender idanS = IdanSender();
  
  return 0;
}





