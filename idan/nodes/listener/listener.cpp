//sender - subsribed to acceleration, braking, steering, indicators, neutralize commands. create a can massage for the Idan drive by wire system 
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <ros/ros.h>
#include "autoware_can_msgs/CANPacket.h"
// #include "autoware_msgs/SteerCmd.h"
// #include "autoware_msgs/AccelCmd.h"
#include <std_msgs/Float64.h>


#include "listener.h"

class IdanListener
{
  public:
  IdanListener();
  ~IdanListener();
private:
  ros::NodeHandle n;
  ros::Subscriber can_sub;
  ros::Publisher accel_pub;
  ros::Publisher steer_pub;  
  //ros::Publisher error_pub;  //TODO

  void canCallback(const autoware_can_msgs::CANPacket::ConstPtr& msg);
  convertIdanData Conv;
  std_msgs::Float64 steer;
  std_msgs::Float64 accel;
  receivingData rData = receivingData();
  void convertFromCan();

};
IdanListener::IdanListener()
{
  can_sub = n.subscribe("can_raw_data", 1000, &IdanListener::canCallback, this);

  steer_pub = n.advertise<std_msgs::Float64>("current_steer", 10);
  accel_pub = n.advertise<std_msgs::Float64>("current_accel", 10);
  ros::spin();
}
IdanListener::~IdanListener()
{
}
void IdanListener::canCallback(const autoware_can_msgs::CANPacket::ConstPtr& msg)
{
  unsigned char idan_rData[8];
 //ROS_INFO_STREAM("callback");

  if (msg->id == 0x39)
  {
    for (int j = 0; j < 8; j++)
    {
      //printf("%2.2x ", idan_rData[j]);
      idan_rData[j] = msg->dat[j];
    }
    Conv.convert_from_idan(idan_rData,&rData);

    
    steer.data = rData.steer;
    steer_pub.publish(steer);
    accel.data = rData.acc;
    accel_pub.publish(accel);
    //ROS_INFO_STREAM("steer: "<<steer.data<<"accel: "<< accel.data);
  }
}



int main (int argc, char *argv[])

{
  ros::init(argc, argv, "idan_listener");
  IdanListener idanS = IdanListener();
  
  return 0;
}





