//sender - subsribed to acceleration, braking, steering, indicators, neutralize commands. create a can massage for the Idan drive by wire system 
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <ros/ros.h>
#include "autoware_can_msgs/CANPacket.h"


#include "can_communication.h"//shared with can_communication_lib.cpp
#include "socketCanComm.h"//shared with socketCanComm.cpp

class CanComm
{
  public:
  CanComm();
  ~CanComm();
  int run();

private:
  ros::NodeHandle n;
  ros::Subscriber can_sub;
  ros::Publisher can_pub;
  bool first_flag = false;//true;

  autoware_can_msgs::CANPacket can_raw_command;  
  autoware_can_msgs::CANPacket can_raw_data;  
  void UpdateDataCallback(const autoware_can_msgs::CANPacket::ConstPtr& msg);

};
CanComm::CanComm()
{
  can_sub = n.subscribe("can_raw_cmd", 1000, &CanComm::UpdateDataCallback, this);
  can_pub = n.advertise<autoware_can_msgs::CANPacket>("can_raw_data", 10);
}
int CanComm::run()
{
////TODO wait for safe commands before connecting to idan
////ros::Duration dt(0.05);
ros::Rate loop_rate(50);
int id = 0,len = 0;
SocketCanComm Comm;
//ROS_INFO_STREAM("before connect" );
//int a = 10;
bool error;
//ROS_INFO_STREAM("before connect1" );
error = Comm.connect();//TODO add all defenition as arguments to function
if (error)
{
  ROS_ERROR_STREAM("cannot connect to CAN");
  return 1;
}

while(ros::ok())// && count < 100
{
  if(!first_flag)
  {
    ROS_INFO_STREAM("read" );
  bool read_failed = Comm.readCan(&id, &can_raw_data.dat[0],&len);
  if (!read_failed)
  {
    can_raw_data.id = id;
    can_raw_data.len = len;
    can_pub.publish(can_raw_data);
  }
  //ROS_INFO_STREAM("len: "<<int(can_raw_command.len) );

  //Comm.writeCan(int(can_raw_command.id), &can_raw_command.dat[0], int(can_raw_command.len));
  
  }
  ros::spinOnce();
  loop_rate.sleep();
  
}
ROS_INFO_STREAM("finish" );
Comm.disconnect();
return 0;
//ros::spin();

}

CanComm::~CanComm()
{
}
void CanComm::UpdateDataCallback(const autoware_can_msgs::CANPacket::ConstPtr& msg)
{
  can_raw_command.id = msg->id;
  can_raw_command.dat = msg->dat;
  can_raw_command.len = int(msg->len);
  for (int j = 0; j < 8; j++)
  {
    //printf("%2.2x ", msg->dat[j]);
	  can_raw_command.dat[j]=msg->dat[j];
	}
  ROS_INFO_STREAM("id: "<<can_raw_command.id );

  first_flag = false;
}



 
int main (int argc, char *argv[])
{
  ros::init(argc, argv, "can_communication");
  CanComm canComm = CanComm();
  canComm.run();
  return 0;
}





