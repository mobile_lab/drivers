#pragma once

//#include <stdio.h>
//#include <stdlib.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <linux/can.h>
#include <linux/can/raw.h>

class SocketCanComm
{
//	TCAN_HANDLE Handle;
//	TCAN_STATUS Status;
//	CAN_MSG SendMSG;
//	CAN_MSG RecvMSG;
    int s;
    struct can_frame frame;
public:
        SocketCanComm();
        ~SocketCanComm();
	bool connect();
        bool writeCan(int id,unsigned char mess[],int size);
        bool readCan(int * id,unsigned char mess[],int * size);
	bool disconnect();

};
