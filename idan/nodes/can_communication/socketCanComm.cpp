//#include <stdio.h>
//#include <stdlib.h>
//#include<iostream>
//#include <ros/ros.h>

#include "socketCanComm.h"

//#include<cstring>


SocketCanComm::SocketCanComm()
{

}
SocketCanComm::~SocketCanComm()
{
	
}
bool SocketCanComm::connect()
{
//	ROS_INFO_STREAM("connect2" );
//	CHAR *ComPort = "/dev/ttyUSB1";
//	CHAR *szBitrate = "500";
//	CHAR *acceptance_code = "1FFFFFFF";
//	CHAR *acceptance_mask = "00000039";//"00000039";
//	VOID *flags = CAN_TIMESTAMP_OFF;
//	DWORD Mode = Normal;
//	char version[10];
//	Handle = -1;
//	Status = 0;
//	SendMSG.Flags = CAN_FLAGS_STANDARD;
//	SendMSG.Id = 0x30;//0x31 blink!
//	SendMSG.Size = 8;
//	ROS_INFO_STREAM("connect3" );
//	Handle = CAN_Open(ComPort, szBitrate, acceptance_code, acceptance_mask, flags, Mode);
//	printf("handle= %d\n", Handle);
//	if (Handle < 0)
//		return true;//error
//	std::memset(version, 0, sizeof(char) * 10);
//	Status = CAN_Flush(Handle);
//	Status = CAN_Version(Handle, version);
//	if (Status == CAN_ERR_OK) {
//		printf("Version : %s\n", version);
//	}
//	return false;


    struct sockaddr_can addr;

    struct ifreq ifr;

    const char *ifname = "can1";

    if((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
        perror("Error while opening socket");
        return -1;
    }

    strcpy(ifr.ifr_name, ifname);
    ioctl(s, SIOCGIFINDEX, &ifr);

    addr.can_family  = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;

    printf("%s at index %d\n", ifname, ifr.ifr_ifindex);

    if(bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        perror("Error in socket bind");
        return -2;
    }


}

bool SocketCanComm::writeCan(int id,unsigned char mess[],int size)
{
        frame.can_id  = id;
        frame.can_dlc = size;
        for (int i = 0; i < size; i++)
                frame.data[i] = mess[i];

        int nbytes = write(s, &frame, sizeof(struct can_frame));

        return false;

}
bool SocketCanComm::readCan(int * id,unsigned char mess[],int * size)
{
	
//	do
//	{
//		Status = CAN_Read(Handle, &RecvMSG);
		
//	}
//	while (RecvMSG.Id != 0x39);// Status == -5 ||

//	if (Status == CAN_ERR_OK)
//	{
//		/*printf("Read ID=0x%X, Type=%s, DLC=%d, FrameType=%s, Data=",
//			RecvMSG.Id, (RecvMSG.Flags & CAN_FLAGS_STANDARD) ? "STD" : "EXT",
//			RecvMSG.Size, (RecvMSG.Flags & CAN_FLAGS_REMOTE) ? "REMOTE" : "DATA");*/

//		*id = RecvMSG.Id;
//		*size = RecvMSG.Size;
//		for (int i = 0; i < RecvMSG.Size; i++)
//		{
//			//printf("%X,", RecvMSG.Data[i]);
//			mess[i] = RecvMSG.Data[i];
//		}
//		//printf("\n");
//		return false;
			
//	}
//	//printf("Error cannot read from can %d \n", Status);
        //return true;


    int nbytes = read(s, &frame, sizeof(struct can_frame));

    if (nbytes < 0) {
            perror("can raw socket read");
            return true;
    }

    /* paranoid check ... */
    if (nbytes < sizeof(struct can_frame)) {
            fprintf(stderr, "read: incomplete CAN frame\n");
            return true;
    }


    *id = frame.can_id;
    *size = frame.can_dlc;
    for (int i = 0; i < frame.can_dlc; i++)
    {
            printf("%X,", frame.data[i]);
            mess[i] = frame.data[i];
    }



        return false;

}

bool SocketCanComm::disconnect()
{
//	printf("disconnect\n");
//	Status = CAN_Close(Handle);
//	printf("disconnect1\n");
//	if (Status == CAN_ERR_OK)
//	{
//		printf("close Success\n");
//		return false;
//	}
//	printf("close Error\n");
//	return true;
    return false;

}
